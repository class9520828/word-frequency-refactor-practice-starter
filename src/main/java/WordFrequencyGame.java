import java.util.*;


public class WordFrequencyGame {

    public static final String SPACEBREAK = "\\s+";
    public static final int INITIAL_LENGTH = 1;

    public String getResult(String inputStr) {

        try {

            List<Input> inputList = getInputList(inputStr);
            List<Input> inputCountList = getInputCountList(inputList);
            return getWordCountResult(inputCountList);

        } catch (Exception e) {

            return "Calculate Error";
        }

    }

    private static String getWordCountResult(List<Input> inputCountList) {
        inputCountList.sort((wordOne, wordTwo) -> wordTwo.getWordCount() - wordOne.getWordCount());
        StringJoiner wordCountResult = new StringJoiner("\n");
        inputCountList
                .forEach(wordCountInput -> wordCountResult.add(wordCountInput.getValue() + " " + wordCountInput.getWordCount()));
        return wordCountResult.toString();
    }


    private static List<Input> getInputCountList(List<Input> inputList) {
        Map<String, List<Input>> listMap = new HashMap<>();
        inputList.forEach(input -> {
            if (listMap.containsKey(input.getValue())) {
                listMap.get(input.getValue()).add(input);
            } else {
                List<Input> classifiedInputList = new ArrayList<>();
                classifiedInputList.add(input);
                listMap.put(input.getValue(), classifiedInputList);
            }
        });

        List<Input> inputCountList = new ArrayList<>();
        listMap.forEach((key, value) -> inputCountList.add(new Input(key, value.size())));
        return inputCountList;
    }


    private static List<Input> getInputList(String inputStr) {
        List<Input> inputList = new ArrayList<>();
        Arrays.stream(inputStr.split(SPACEBREAK)).forEach(shortStr -> inputList.add(new Input(shortStr, INITIAL_LENGTH)));
        return inputList;
    }

}

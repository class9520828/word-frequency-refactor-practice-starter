O(Objective):
+ In the morning, I learned the ideas, application scenarios, and implementation of the three modes of Observer pattern, Command Mode, and Strategy pattern through the sharing activity between groups.
+ In the afternoon, I learned the specifications of Refactor, including the types of code that need to be refactored, the principles that need to be followed during refactoring, and the specific steps for refactoring.
+ I did exercises on Code refactoring in class, which helped me gradually develop the correct habit of Refactor and become familiar with the steps of Refactor.

R(Reflective):  
&emsp;&emsp;Fruitful

I(Interpretive):  
&emsp;&emsp; I am not yet proficient in accurately identifying method structures, variable naming, and loop structures that require reconstruction, which requires further practice in the future. &emsp;I hope to quickly identify problems and propose reconstruction methods in the future.

D(Decision):

&emsp;&emsp;Continue to practice the specifications and methods of Code refactoring. &emsp;Train yourself to be sensitive to non-standard code. &emsp;Pay attention to standardized writing when programming.
